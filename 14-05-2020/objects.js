//JSON - JavaScript Object Notation
var Item = {'name': 'Bottle', 'size': 1000, 'capacity': 1000, drinkWater: function(){this.capacity = this.capacity - 10} }

console.log(Item);

console.log(Item.drinkWater());

console.log(Item);

var Item1 = Item;

console.log(Item1);

var Item = function Item(){
	this.name = "Bottle";
	this.size = 1000;
	this.capacity = '1000'
	
	this.drinkWater = function(){
		this.capacity = this.capacity - 10;
	}
}

var Item2 = new Item();
Item2.drinkWater();
console.log(Item2, 'Item2');

var Item3 = new Item();
console.log(Item3, 'item3');

