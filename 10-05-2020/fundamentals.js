// Setup Instructions
// 1. IDE - VS Code / WebStorm
// 2. Node software for running the javascript code 

// String interpolation
console.log(`program started time is --> ${new Date()}`);

let discountPercent = 10;
const taxChargePercent = 18;
const allowedCountry = 'India';

// Data Type -> 'Harish', 'A', '12', 12.23, null, undefined
// 12.23 + 5.6 => 17.83
// '12.23' + 5.6 => '12.235.6'
// parseInt('12.23') => 12

console.log(12.23 + 5.6);

console.log('12.23' + 5.6);

console.log(parseFloat('12.23') + 5.6);
console.log(parseInt('12.23') + 5.6);

console.log(11110000 + 10);
console.log((11110000).toString() + (10).toString()); // 1111000010

items = ['apples', 'mangos', 'papaya'];
itemPrices = [23, 12, 15];
itemMaxQunatity = [10, 10, 4];
purchasesItems = [5, 4, 3];
purchasesItems2 = [0, 2, 0];

// Conditional statements - if - [else]/ switch
// Loop statements - for / while 

function printDashes(length = 30, format = '-'){
    let dashes = '';
    for(let index = 0; index < length; index++){
        dashes = dashes + format;
    }

    console.log(dashes);
}

printDashes(undefined, '=');
console.log('Billing Details');
printDashes(undefined, '=');

let itemsTotal = 0;

for(let index = 0 ; index < purchasesItems.length; index++){
    let quantity = purchasesItems[index];
    let maxQuantity = itemMaxQunatity[index];
    let price = itemPrices[index];
    let itemName = items[index];
    let itemCapitalName = (itemName.charAt(0).toUpperCase() + itemName.slice(1))

    let itemTotal = quantity * price;
    itemsTotal = itemsTotal + itemTotal;
    console.log(`${itemCapitalName} - ${quantity} * ${price} - ${itemTotal}`);
}

const discountToCustomer = itemsTotal * (discountPercent / 100);
const itemsTotalAfterDiscount = itemsTotal - Math.round(discountToCustomer);
const taxDeduction = Math.round(itemsTotalAfterDiscount * (taxChargePercent / 100));
const finalTotal = Math.round(itemsTotalAfterDiscount + taxDeduction);

printDashes(20, '-')

console.log('Items Total - ', itemsTotal);
console.log(`Discount (${discountPercent}%) `, 
            `-> (${itemsTotal} - ${Math.round(discountToCustomer)}) `,
            `-> ${itemsTotalAfterDiscount}(Discounted Amount - ${discountToCustomer})`);
console.log(`Tax(${taxChargePercent}%) - `, taxDeduction);
console.log(`Total - ${finalTotal}`);

printDashes(undefined, '=');

/*
--------------------------
Billing details 
--------------------------
Apples - 5 * 23 - 115
Mangos - 4 * 12 - 48
Papaya - 3 * 15 - 45

Items Total - 208
Discount (10%) -> (208 - 21) -> 187(Discounted Amount - 20.8)
Tax(18%) - 34
Total - 221
---------------------------
*/
